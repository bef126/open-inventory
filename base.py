import jinja2
import webapp2
import os.path
from google.appengine.api import users

class BaseHandler(webapp2.RequestHandler):
  @webapp2.cached_property
  def jinja_environment(self):
    template_path = os.path.abspath(os.path.join(os.path.dirname( __file__ ), 'templates'))
    return jinja2.Environment(
      loader=jinja2.FileSystemLoader(template_path),
      extensions=['jinja2.ext.autoescape'],
      autoescape=True
    )

  def render_template(self, view_filename, params=None):
    if not params:
      params = {}
    
    u = users.get_current_user()
    if u:
      params['email'] = u.email()
    else:
      params['email'] = ''

    # highlight correct link in top nav bar
    url = self.request.url
    homeClass = ""
    inventoryClass = ""
    if "p/inventory" in url:
      inventoryClass = "active"
    else:
      homeClass = "active"

    params['homeClass'] = homeClass
    params['inventoryClass'] = inventoryClass
    
    template = self.jinja_environment.get_template(view_filename)
    self.response.write(template.render(params))