from base import BaseHandler
import webapp2
from models import *
from google.appengine.api import users

class InventoryHandler(BaseHandler):
  def get(self):
    results = []
    try:
      u = users.get_current_user()
      results = Product.all_for_owner(u.email())
    except:
      results = []
    params = {'results': results}
    self.render_template('inventory.html', params)

class UpdateNameHandler(BaseHandler):
  def post(self):
    newName = self.request.get("value")
    key = self.request.get("pk")
    p = Product.find_by_guid(key)
    p.name = newName
    p.put()
    self.response.write('OK')

application = webapp2.WSGIApplication([
    ('/p/inventory', InventoryHandler),
    ('/p/uname', UpdateNameHandler)
], debug=True)