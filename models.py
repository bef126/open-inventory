from google.appengine.ext import ndb

class Base(ndb.Model):
  created_at = ndb.DateTimeProperty(auto_now_add=True)
  updated_at = ndb.DateTimeProperty(auto_now=True)

  def guid(self):
    return self.key.urlsafe()

  @classmethod
  def find_by_guid(cls, guid):
    key = ndb.Key(urlsafe=guid)
    return key.get()

class Product(Base):
  upc = ndb.StringProperty(required=True)
  name = ndb.StringProperty(required=True)
  owner = ndb.StringProperty(required=True)
  inventory = ndb.IntegerProperty(default=0)

  @classmethod
  def lookup_by_upc_and_owner(cls, upc, owner):
    return cls.query(cls.owner == owner).filter(cls.upc == upc).get()

  @classmethod
  def name_suggestions(cls, upc):
    result = []
    products = cls.query(cls.upc == upc).fetch(10)
    for product in products:
      result.append(product.name)
    return list(set(result))

  @classmethod
  @ndb.transactional
  def increment_inventory(cls, key, delta):
    p = cls.find_by_guid(key)
    if p:
      p.inventory = p.inventory + delta
      p.put()
      return p

  @classmethod
  @ndb.transactional
  def decrement_inventory(cls, key, delta):
    p = cls.find_by_guid(key)
    if p:
      if p.inventory >= delta:
        p.inventory = p.inventory - delta
        p.put()
        return p

  @classmethod
  def all_for_owner(cls, owner):
    return cls.query(cls.owner == owner).order(cls.name).fetch(1000)