from base import BaseHandler
import webapp2

class HomePageHandler(BaseHandler):
  def get(self):
    self.render_template('home.html', {})

application = webapp2.WSGIApplication([
    ('/', HomePageHandler)
], debug=True)