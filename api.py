import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote

import models
import secret

package = 'OpenInventory'

# ProductExists
class ProductExistsRequest(messages.Message):
  upc = messages.StringField(1, required=True)

class ProductExistsResponse(messages.Message):
  pkey = messages.StringField(1)
  inventory = messages.IntegerField(2)
  name = messages.StringField(3)

# CreateProduct
class CreateProductRequest(messages.Message):
  upc = messages.StringField(1, required=True)
  name = messages.StringField(2, required=True)
  inventory = messages.IntegerField(3, required=True)

# UpdateInventory
class UpdateInventoryRequest(messages.Message):
  pkey = messages.StringField(1, required=True)
  delta = messages.IntegerField(2, required=True)

class UpdateInventoryResponse(messages.Message):
  inventory = messages.IntegerField(1)
  name = messages.StringField(2)

# Suggestions
class SuggestionsResponse(messages.Message):
  suggestions = messages.StringField(1, repeated=True)

# My Inventory
class MyInventoryResponse(messages.Message):
  products = messages.MessageField(ProductExistsResponse, 1, repeated=True)


@endpoints.api(
  name='openinventory',
  version='v1',
  allowed_client_ids=[secret.WEB_CLIENT_ID, secret.ANDROID_CLIENT_ID_DEV, secret.ANDROID_CLIENT_ID_PROD, endpoints.API_EXPLORER_CLIENT_ID],
  audiences=[secret.ANDROID_AUDIENCE],
  scopes=[endpoints.EMAIL_SCOPE]
)
class OpenInventoryApi(remote.Service):
  
  @endpoints.method(ProductExistsRequest, ProductExistsResponse,
                    http_method='GET',
                    name='productExists')
  def product_exists(self, request):
    current_user = endpoints.get_current_user()
    if current_user is None:
      raise endpoints.UnauthorizedException('Invalid token.')
    email = current_user.email()
    p = models.Product.lookup_by_upc_and_owner(request.upc, email)
    if p:
      return ProductExistsResponse(
        pkey=p.guid(),
        inventory=p.inventory,
        name=p.name
      )
    else:
      raise endpoints.NotFoundException()
    
  @endpoints.method(CreateProductRequest, message_types.VoidMessage,
                    http_method='GET',
                    name='createProduct')
  def product_create(self, request):
    current_user = endpoints.get_current_user()
    if current_user is None:
      raise endpoints.UnauthorizedException('Invalid token.')
    email = current_user.email()
    p = models.Product(
      upc=request.upc,
      name=request.name,
      owner=email,
      inventory=request.inventory
    )
    p.put()
    return message_types.VoidMessage()

  @endpoints.method(UpdateInventoryRequest, UpdateInventoryResponse,
                    http_method='GET',
                    name='incrementInventory')
  def increment_inventory(self, request):
    current_user = endpoints.get_current_user()
    if current_user is None:
      raise endpoints.UnauthorizedException('Invalid token.')
    email = current_user.email()
    p = models.Product.increment_inventory(request.pkey, request.delta)
    if p:
      return UpdateInventoryResponse(
        inventory=p.inventory,
        name=p.name
      )
    else:
      raise endpoints.NotFoundException()

  @endpoints.method(UpdateInventoryRequest, UpdateInventoryResponse,
                    http_method='GET',
                    name='decrementInventory')
  def decrement_inventory(self, request):
    current_user = endpoints.get_current_user()
    if current_user is None:
      raise endpoints.UnauthorizedException('Invalid token.')
    email = current_user.email()
    p = models.Product.decrement_inventory(request.pkey, request.delta)
    if p:
      return UpdateInventoryResponse(
        inventory=p.inventory,
        name=p.name
      )
    else:
      raise endpoints.NotFoundException()

  @endpoints.method(ProductExistsRequest, SuggestionsResponse,
                    http_method='GET',
                    name='suggestName')
  def suggest_name(self, request):
    current_user = endpoints.get_current_user()
    if current_user is None:
      raise endpoints.UnauthorizedException('Invalid token.')
    return SuggestionsResponse(
      suggestions=models.Product.name_suggestions(request.upc)
    )

  @endpoints.method(message_types.VoidMessage, MyInventoryResponse,
                    http_method='GET',
                    name='myInventory')
  def my_inventory(self, request):
    current_user = endpoints.get_current_user()
    if current_user is None:
      raise endpoints.UnauthorizedException('Invalid token.')
    email = current_user.email()
    p = models.Product.all_for_owner(email)
    responses = []
    for x in p:
      a = ProductExistsResponse(
        pkey=x.guid(),
        inventory=x.inventory,
        name=x.name
      )
      responses.append(a)
    return MyInventoryResponse(
      products=responses
    )

application = endpoints.api_server([OpenInventoryApi])
